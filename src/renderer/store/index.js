import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const mutations = {}
const state = {}
const getters = {}


export const store = new Vuex.Store({
  mutations,
  state,
  getters,
})
